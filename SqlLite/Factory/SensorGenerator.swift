//
//  SensorGenerator.swift
//  SqlLite
//
//  Created by admin on 25/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

final class SensorGenerator: NSObject {
	
	static let shared = SensorGenerator()
	
	func generate(number: Int) -> Sensor{
		let sensorName: String
		
		if number < 10 {
			sensorName = "0\(number)"
		}
		else{
			sensorName = String(number)
		}
		let sensor = Sensor(name: NSString(string: sensorName), description: NSString(string: "Sensor number \(number)"))
		return sensor
	}
}
