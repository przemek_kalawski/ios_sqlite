//
//  RecordsFactory.swift
//  SqlLite
//
//  Created by admin on 25/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

final class RecordsFactory: NSObject {
	
	static let shared = RecordsFactory()
	
	let sensors: [Sensor] = SensorsFactory.shared.sensors
	
	func getRecords(quantity: Int) -> [Record] {
		var records: [Record] = []
		for _ in 0..<quantity {
			records.append(getRecord())
		}
		return records
	}
	
	fileprivate func getRecord() -> Record {
		let record = Record(value: Float.random(in: 0...100), sensor: sensors[Int.random(in: 0..<20)], timestamp: 1)
		return record
	}
}
