//
//  SensorsFactory.swift
//  SqlLite
//
//  Created by admin on 25/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

final class SensorsFactory: NSObject {
	
	let sensorGenerator: SensorGenerator = SensorGenerator.shared
	static let shared = SensorsFactory()
	
	var sensors: [Sensor]!
	
	override init(){
		super.init()
		getSensors(quantity: 20)
	}
	
	fileprivate func getSensors(quantity: Int){
		sensors = []
		for index in 1...quantity {
			sensors.append(sensorGenerator.generate(number: index))
		}
	}
}
