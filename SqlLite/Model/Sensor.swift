//
//  Sensor.swift
//  SqlLite
//
//  Created by admin on 24/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation


struct Sensor {
	let name : NSString
	let description: NSString
}

extension Sensor : SQLTable{
	static var createStatement: String {
		return """
		CREATE TABLE Sensor(
		name CHAR(255) PRIMARY KEY NOT NULL,
		description CHAR(255)
		);
		"""
	}
}
