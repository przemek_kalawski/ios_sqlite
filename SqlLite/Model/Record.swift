//
//  Reading.swift
//  SqlLite
//
//  Created by admin on 24/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

struct Record {
	let value: Float
	let sensor: Sensor
	let timestamp: Int32
}

extension Record: SQLTable{
	static var createStatement: String {
		return """
		CREATE TABLE Record(
		id INT PRIMARY KEY AUTOINCREMENT NOT NULL,
		value REAL,
		timestamp INT,
		name CHAR(255),
		FOREIGN KEY (name) REFERENCES Sensor(name)
		);
		"""
	}
}
