//
//  ViewController.swift
//  SqlLite
//
//  Created by admin on 24/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SQLite3

class ViewController: UIViewController {

	let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
	
	@IBOutlet weak var numberTextField: UITextField!
	let recordFactory: RecordsFactory = RecordsFactory.shared
	
	var sqliteDB: SQLiteDB!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		openDB()
		createTables()
		insertSensors()
	}
	
	func openDB(){
		let dbFilePath = NSURL(fileURLWithPath: docDir).appendingPathComponent("sqlite.db")?.path
		do {
			sqliteDB = try SQLiteDB.open(path: dbFilePath!)
			print("Successfully opened connection to database.")
		} catch {
			print("Unable to open database. Verify that you created the directory described in the Getting Started section.")
		}
	}
	
	func createTables() {
		do {
			try sqliteDB.createTable(table: Sensor.self)
		} catch {
			print(sqliteDB.errorMessage)
		}
		
		do {
			try sqliteDB.createTable(table: Record.self)
		} catch {
			print(sqliteDB.errorMessage)
		}
	}
	
	func insertSensors(){
		for sensor in recordFactory.sensors {
			do {
				try sqliteDB.insertSensor(sensor: sensor)
			} catch {
				print(sqliteDB.errorMessage)
			}
		}
	}
	
	@IBAction func addRecords(_ sender: Any) {
		guard let text = numberTextField.text else { return }
		guard let number = Int(text) else { return }
		
		let records = recordFactory.getRecords(quantity: number)
		
		do {
			for record in records {
				try sqliteDB.insertRecord(record: record)
			}
		} catch {
			print(sqliteDB.errorMessage)
		}
		
	}
	@IBAction func deleteAllRecords(_ sender: Any) {
	}
	
}

