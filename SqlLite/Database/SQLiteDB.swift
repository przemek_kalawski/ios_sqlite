//
//  SQLiteDB.swift
//  SqlLite
//
//  Created by admin on 25/09/2018.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import SQLite3

class SQLiteDB {
	fileprivate let dbPointer: OpaquePointer?
	
	fileprivate init(dbPointer: OpaquePointer?){
		self.dbPointer = dbPointer
	}
	
	deinit {
		sqlite3_close(dbPointer)
	}
	
	var errorMessage: String {
		if let errorPointer = sqlite3_errmsg(dbPointer) {
			let errorMessage = String(cString: errorPointer)
			return errorMessage
		} else {
			return "No error message provided from sqlite."
		}
	}
	
	static func open(path: String) throws -> SQLiteDB {
		var db: OpaquePointer? = nil
		// 1
		if sqlite3_open(path, &db) == SQLITE_OK {
			// 2
			return SQLiteDB(dbPointer: db)
		} else {
			// 3
			defer {
				if db != nil {
					sqlite3_close(db)
				}
			}
			
			if let errorPointer = sqlite3_errmsg(db) {
				let message = String.init(cString: errorPointer)
				throw SQLiteError.OpenDatabase(message: message)
			} else {
				throw SQLiteError.OpenDatabase(message: "No error message provided from sqlite.")
			}
		}
	}
}

extension SQLiteDB {
	func prepareStatement(sql: String) throws -> OpaquePointer? {
		var statement: OpaquePointer? = nil
		guard sqlite3_prepare_v2(dbPointer, sql, -1, &statement, nil) == SQLITE_OK else {
			throw SQLiteError.Prepare(message: errorMessage)
		}
		
		return statement
	}
}

extension SQLiteDB {
	func createTable(table: SQLTable.Type) throws {
		// 1
		let createTableStatement = try prepareStatement(sql: table.createStatement)
		// 2
		defer {
			sqlite3_finalize(createTableStatement)
		}
		// 3
		guard sqlite3_step(createTableStatement) == SQLITE_DONE else {
			throw SQLiteError.Step(message: errorMessage)
		}
		print("\(table) table created.")
	}
}

extension SQLiteDB {
	func insertSensor(sensor: Sensor) throws {
		let insertSql = "INSERT INTO Sensor(name, description) VALUES (?, ?);"
		let insertStatement = try prepareStatement(sql: insertSql)
		defer {
			sqlite3_finalize(insertStatement)
		}
		
		let name: NSString = sensor.name
		let description: NSString = sensor.description
		guard sqlite3_bind_text(insertStatement, 1, description.utf8String, -1, nil) == SQLITE_OK  &&
			sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil) == SQLITE_OK else {
				throw SQLiteError.Bind(message: errorMessage)
		}
		
		guard sqlite3_step(insertStatement) == SQLITE_DONE else {
			throw SQLiteError.Step(message: errorMessage)
		}
		
		print("Successfully inserted row.")
	}
	
	func insertRecord(record: Record) throws {
		let insertSql = "INSERT INTO Record(value, name, timestamp) VALUES (?, ?, ?);"
		let insertStatement = try prepareStatement(sql: insertSql)
		defer {
			sqlite3_finalize(insertStatement)
		}
		
		let value: Double = Double(exactly: record.value)!
		let name: NSString = record.sensor.name
		let timestamp: Int32 = record.timestamp
		guard sqlite3_bind_double(insertStatement, 1, value) == SQLITE_OK  &&
			sqlite3_bind_text(insertStatement, 2, name.utf8String, -1, nil) == SQLITE_OK &&
			sqlite3_bind_int(insertStatement, 3, timestamp) == SQLITE_OK else {
				throw SQLiteError.Bind(message: errorMessage)
		}
		
		guard sqlite3_step(insertStatement) == SQLITE_DONE else {
			throw SQLiteError.Step(message: errorMessage)
		}
		
		print("Successfully inserted row.")
	}
}

enum SQLiteError: Error {
	case OpenDatabase(message: String)
	case Prepare(message: String)
	case Step(message: String)
	case Bind(message: String)
}

protocol SQLTable {
	static var createStatement: String { get }
}
